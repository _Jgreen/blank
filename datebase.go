package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	_ "github.com/lib/pq" // _ needed
)

type sqlInfo struct {
	Host    string `json:"host"`
	User    string `json:"user"`
	Pass    string `json:"pass"`
	Dbname  string `json:"dbname"`
	Sslmode string `json:"sslmode"`
}

// DB is exported db
type DB struct {
	*sql.DB
}

// NewOpen exported for creating a new Postgresql instance
func NewOpen() (DB, error) {
	var info sqlInfo
	if bytes, err := ioutil.ReadFile("info.json"); err != nil {
		panic(err)
	} else {
		err2 := json.Unmarshal(bytes, &info)
		if err2 != nil {
			panic(err2)
		}
	}
	dbInfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		info.Host, info.User, info.Pass, info.Dbname)
	db, err := sql.Open("postgres", dbInfo)
	if err != nil {
		log.Fatal("Invalid DB config:", err)
	}
	if err = db.Ping(); err != nil {
		log.Fatal("DB unreachable:", err)
	}

	return DB{db}, err
}

func (db *DB) getUserByDB(no int) (*User, error) {
	return nil, nil
}

func (db *DB) getDocByDB(no int) (*Doc, error) {
	return nil, nil
}

func (db *DB) getProjByDB(no int) (*Project, error) {
	return nil, nil
}

func (db *DB) getProjNameByDb(no int) string {
	var name string
	row, err := db.Query(fmt.Sprintf(`SELECT title FROM "projects" WHERE no=%d;`, no))
	if err != nil {
		println("getProjNameByDb:", err.Error())
	}
	if row.Next() {
		row.Scan(&name)
	}
	defer row.Close()

	return name
}

func (db *DB) getDocsForProj(no int) []Doc {
	var docs []Doc
	if rows, err := db.Query(fmt.Sprintf(`
	SELECT no,title FROM "docs" WHERE parent=%d;`, no)); err != nil {
		println("getDocsForProj:", err.Error())
	} else {
		for rows.Next() {
			doc := Doc{}
			doc.Parent = no
			rows.Scan(&doc.No, &doc.Title)
			docs = append(docs, doc)
		}
		defer rows.Close()
	}
	return docs
}

func (db *DB) getUsersForProj(no int) ([]ProjectUser, error) {
	var users []ProjectUser
	if rows, err := db.Query(fmt.Sprintf(`
	SELECT id FROM "user_and_project" INNER JOIN "users" ON project_key=%d;`, no)); err != nil {
		println("getUsersForProj:", err.Error())
	} else {
		for rows.Next() {
			user := ProjectUser{}
			rows.Scan(&user.U.Id)
			users = append(users, user)
		}
		defer rows.Close()
	}
	return users, nil
}

func (db *DB) getDoc(doc *Doc) {
	row, err := db.Query(fmt.Sprintf(`
			SELECT no,title FROM "docs" WHERE parent=%d AND no=%d`, doc.Parent, doc.No))
	if err != nil {
		println("getDoc1:", err.Error())
	}
	if row.Next() { // 문서가 이써
		row.Scan(&doc.No, &doc.Title)

		rows, err2 := db.Query(fmt.Sprintf(`
		SELECT title,contents FROM "doc_contents" WHERE doc_key=%d ORDER BY no`, doc.No))
		if err2 != nil {
			println("getDoc2", err2.Error())
		}
		for rows.Next() {
			var d DocContents
			rows.Scan(&d.Title, &d.Contents)
			doc.Contents = append(doc.Contents, d)
		}
		rows.Close()
	}
	defer row.Close()
}
