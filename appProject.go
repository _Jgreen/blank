package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

/*PROEJCT side*/
func (s *Service) projViewHandler(w http.ResponseWriter, r *http.Request, u *User) {
	v := mux.Vars(r)
	var proj Project
	isProj := false
	if rows, err := s.db.Query(
		fmt.Sprintf(
			`SELECT no,title,contents,read_auth,write_auth FROM "projects" WHERE no='%s'`,
			v["proj"])); err != nil {
		println("projView/readProj", err.Error())
	} else {
		if rows.Next() {
			rows.Scan(&proj.No, &proj.Title, &proj.Contents, &proj.Read, &proj.Write)
			isProj = true
		}
		defer rows.Close()
	}
	if !isProj {
		s.errorMsg(w, r)
		return
	}

	_, a := s.docCheckReadAuth(u, v["proj"])
	if !a {
		s.authMsg(w, r)
		return
	}

	// doc list 불러와서 ㅅㅅ하기
	proj.Authors, _ = s.db.getUsersForProj(proj.No)
	proj.Docs = s.db.getDocsForProj(proj.No)
	proj.User = *u
	if err := s.tmpls["project"].Execute(w, proj); err != nil {
		println(err.Error())
	}
}

func (s *Service) projEditHandler(w http.ResponseWriter, r *http.Request, u *User) {
	if s.isAuth(u, 3) {
		s.tmpls["newProject"].Execute(w, nil)
	} else {
		s.authMsg(w, r)
	}
}

func (s *Service) projSaveHandler(w http.ResponseWriter, r *http.Request, u *User) {
	if !s.isAuth(u, 3) {
		s.authMsg(w, r)
		return
	}
	r.ParseForm()
	users := []ProjectUser{}
	t := r.FormValue("title")
	if rows, err := s.db.Query(fmt.Sprintf(`
	SELECT count(*) FROM "projects" where title='%s'`, t)); err != nil {
		println("51 line", err.Error())
	} else {
		if rows.Next() {
			i := 0
			rows.Scan(&i)
			if i != 0 {
				//
				fmt.Fprintf(w, `
				<script>
					alert("이미존재하는 프로젝트 이름입니다.");
					location="/p";
				</script>`)
				return
			}
		}
		defer rows.Close()
	}
	read, _ := strconv.Atoi(r.FormValue("readAuth"))
	write, _ := strconv.Atoi(r.FormValue("writeAuth"))
	p := Project{
		Title:    t,
		Contents: r.FormValue("contents"),
		Authors:  append(users, ProjectUser{U: *u, W: 1}),
		Write:    write,
		Read:     read,
	}

	if _, err := s.db.Exec(fmt.Sprintf(`
	INSERT INTO "projects" (title,contents,read_auth,write_auth) VALUES ('%s','%s',%d,%d)`,
		p.Title, p.Contents, p.Read, p.Write)); err != nil {
		println("projsave/InsertProj:", err.Error())
	}
	if rows, err := s.db.Query(fmt.Sprintf(`
	SELECT no FROM "projects" WHERE title='%s'`, p.Title)); err != nil {
		println("projsave/수정하자.", err.Error())
	} else {
		if rows.Next() {
			rows.Scan(&p.No)
		}
		defer rows.Close()
	}
	if _, err := s.db.Exec(fmt.Sprintf(`
	INSERT INTO "user_and_project"(project_key,users_key,type) VALUES (%d,%d,1)`, p.No, u.No)); err != nil {
		println("saveProj/user_and_project:", err.Error())
	}

	fmt.Fprintf(w, `
	<script>
		location = "/blank/%d"
	</script>`, p.No)
}

func (s *Service) docViewHandler(w http.ResponseWriter, r *http.Request, u *User) {
	v := mux.Vars(r)
	p, a := s.docCheckReadAuth(u, v["proj"])
	if !a {
		s.authMsg(w, r)
		return
	}
	doc := Doc{Parent: p}
	no, err := strconv.Atoi(v["doc"])
	if err != nil {
		s.errorMsg(w, r)
		return
	}
	doc.No = no
	s.db.getDoc(&doc)
	doc.User = *u
	obj := DocTmpl{
		Title: s.db.getProjNameByDb(p),
		Doc:   doc,
		Docs:  s.db.getDocsForProj(p),
	}

	if err2 := s.tmpls["doc"].Execute(w, obj); err2 != nil {
		println(err2.Error())
	}
}

/*DOCUMENT side*/
func (s *Service) docEditHandler(w http.ResponseWriter, r *http.Request, u *User) {
	v := r.URL.Query() //p=112&d=123
	p, a := s.docCheckWriteAuth(u, v.Get("p"))
	if !a {
		s.authMsg(w, r)
		return
	}
	doc := Doc{Parent: p}
	no, err := strconv.Atoi(v.Get("d"))
	if err == nil {
		doc.No = no
		s.db.getDoc(&doc)
	}
	if err2 := s.tmpls["docEditor"].Execute(w, doc); err2 != nil {
		println(err2.Error())
	}
}

func (s *Service) docSaveMethod(w http.ResponseWriter, r *http.Request, u *User) {
	v := r.URL.Query() //p=112&d=123
	p, a := s.docCheckWriteAuth(u, v.Get("p"))
	if !a {
		s.authMsg(w, r)
		return
	}
	r.ParseForm()
	title := r.FormValue("title")
	subtitle := r.PostForm["t"]
	contents := r.PostForm["c"]
	doc := Doc{Parent: p}

	if no, err := strconv.Atoi(v.Get("d")); err == nil {
		row, err2 := s.db.Query(fmt.Sprintf(`SELECT no,title FROM "docs" WHERE no=%d`, no))
		if err2 != nil {
			println("docSave/doc찾는중 :", err2.Error())
		}
		if row.Next() {
			row.Scan(&doc.No, &doc.Title)
		}
		defer row.Close()
	}

	if doc.Title != "" { // 없음
		if _, err := s.db.Exec(fmt.Sprintf(`UPDATE "docs" set title='%s', last_date=now() where no=%d`, title, doc.No)); err != nil {
			println("docsave/Update:", err.Error())
		}
		if row, err := s.db.Query(fmt.Sprintf(`
		SELECT count(*) FROM "user_and_doc" WHERE user_key=%d`, u.No)); err != nil {
			println("line179", err.Error())
		} else {
			var isUser int
			if row.Next() {
				row.Scan(&isUser)
			}
			defer row.Close()
			if isUser == 0 {
				if _, err2 := s.db.Exec(fmt.Sprintf(`
				INSERT INTO "user_and_doc"(doc_key,user_key,type) VALUES (%d,%d,2)`, doc.No, u.No)); err2 != nil {
					println("docsave/user_and_doc추가중 : ", err2.Error())
				}
			}
		}
	} else {
		if row, err := s.db.Query(fmt.Sprintf(`SELECT last_value from "docs_no_seq"`)); err != nil {
			println("docsave/seq_select :", err.Error())
		} else {
			if row.Next() {
				row.Scan(&doc.No)
				doc.No++
			}
			defer row.Close()
		}

		if _, err := s.db.Exec(fmt.Sprintf(`INSERT INTO "docs"(title,parent) VALUES ('%s',%d)`, title, doc.Parent)); err != nil {
			println("docsave/newdoc :", err.Error())
		}

		if _, err := s.db.Exec(fmt.Sprintf(`INSERT INTO "user_and_doc"(doc_key,user_key,type) VALUES (%d,%d,1)`,
			doc.No, u.No)); err != nil {
			println("docsave/insert user_and_doc:", err.Error())
		}
	}

	if _, err2 := s.db.Exec(fmt.Sprintf(`DELETE FROM "doc_contents" WHERE doc_key=%d`, doc.No)); err2 != nil {
		println("line198:", err2.Error())
	}
	for i := 0; i < len(subtitle); i++ {
		_, err2 := s.db.Exec(fmt.Sprintf(`INSERT INTO "doc_contents"(doc_key,no,title,contents) VALUES (%d,%d,'%s','%s')`, doc.No, i, subtitle[i], contents[i]))
		if err2 != nil {
			println("line201:", err2.Error())
		}
	}
	if _, err := s.db.Exec(fmt.Sprintf(`
	insert into "user_and_project"
	select %d,%d,2 from "user_and_project"
	where not exists (select 1 from "user_and_project" where users_key=%d AND project_key=%d);
	`, u.No, doc.Parent, u.No, doc.Parent)); err != nil {
		println("docsave/user_and_project :", err.Error())
	}
	http.Redirect(w, r, fmt.Sprintf("blank/%d/%d", doc.Parent, doc.No), 302)
}

/*	sql 구문
	SELECT [columms] From "[tables]"
	INSERT INTO "[table]"([columms]...) VALUES ([칼럼에 맞는 값]...)
	UPDATE [table] SET [columms] = [값]
	DELETE FROM [table]
*/

func (s *Service) isAuth(u *User, auth int) bool {
	var userAuth int
	if u.No == -1 {
		userAuth = 7
	} else {
		userAuth = u.Auth
	}
	return userAuth <= auth
}

func (s *Service) docCheckAuth(u *User, projNo string, readORwrite bool) (int, bool) { //r=0, w=1
	no, err := strconv.Atoi(projNo)
	if err != nil {
		return 0, false
	}
	rows, err2 := s.db.Query(fmt.Sprintf(`SELECT read_auth,write_auth FROM "projects" WHERE no=%d`, no))
	if err2 != nil {
		println("isAuth:", err2.Error())
		return 0, false
	}
	if !rows.Next() {
		return 0, false
	}
	var read, write int
	if err3 := rows.Scan(&read, &write); err3 != nil {
		return 0, false
	}
	defer rows.Close()

	if readORwrite {
		return no, s.isAuth(u, write)
	}
	return no, s.isAuth(u, read)
}

func (s *Service) docCheckReadAuth(u *User, projNo string) (int, bool) {
	return s.docCheckAuth(u, projNo, false)
}

func (s *Service) docCheckWriteAuth(u *User, projNo string) (int, bool) {
	return s.docCheckAuth(u, projNo, true)
}
