package main

import (
	"net/http"
)

func main() {

	handler := NewService()
	handler.SetupApp()
	http.ListenAndServe(":80", handler.r)
}

/*
/						-> main.html
/signin					-> login.html
///signin.GET
/signup					-> register.html
///signup.GET
/p/{project}			-> project.html
/p/{project}/{doc}		-> doc.html
/b/{board}				-> board.html
/b/{board}/{post}		-> threads.html
/u						-> user.html
/u/{userID}				-> profile.html
/
/
*/
