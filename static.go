package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
)

//main handler//

func (s *Service) mainHandler(w http.ResponseWriter, r *http.Request, u *User) {
	count := []int{0, 0, 0}
	tables := []string{"projects", "docs", "users"}
	for i, v := range tables {
		ss := fmt.Sprintf(`select count(*) from "%s";`, v)
		if rows, e := s.db.Query(ss); e == nil {
			defer rows.Close()
			for rows.Next() {
				if e2 := rows.Scan(&count[i]); e2 != nil {
					println("에러 :", e2.Error())
				}
			}
		} else {
			println("에러 :", e.Error())
		}

	}
	d := MainTmpl{
		P:    count[0],
		D:    count[1],
		U:    count[2],
		User: *u}

	rows, err := s.db.Query(fmt.Sprintf(`
	SELECT no,title FROM "projects" WHERE read_auth >= %d`, u.Auth))
	if err != nil {
		println(err.Error())
		return
	}
	for rows.Next() {
		var p Project
		if err2 := rows.Scan(&p.No, &p.Title); err2 != nil {
			println(err2.Error())
		}
		d.Projs = append(d.Projs, p)
	}
	defer rows.Close()
	if err2 := s.tmpls["main"].Execute(w, d); err2 != nil {
		println(err2.Error())
	}
}

//static handler//
func (s *Service) staticHandler(w http.ResponseWriter, r *http.Request) {
	localPath := r.URL.Path[1:]
	content, err := ioutil.ReadFile(localPath)
	if err != nil {
		w.WriteHeader(404)
		w.Write([]byte(http.StatusText(404)))
		return
	}
	contentType := getContentType(localPath)
	w.Header().Add("Content-Type", contentType)
	w.Write(content)
}

func getContentType(localPath string) string {
	var contentType string
	ext := filepath.Ext(localPath)

	switch ext {
	case ".html":
		contentType = "text/html"
	case ".css":
		contentType = "text/css"
	case ".js":
		contentType = "application/javascript"
	case ".png":
		contentType = "image/png"
	case ".jpg":
		contentType = "image/jpeg"
	default:
		contentType = "text/plain"
	}
	return contentType
}

func (s *Service) errorMsg(w http.ResponseWriter, r *http.Request) {
	s.tmpls["msg"].Execute(w, msg{
		Title: "에러!",
		Msg:   "잘못된 접근입니다.",
	})
}

func (s *Service) authMsg(w http.ResponseWriter, r *http.Request) {
	s.tmpls["msg"].Execute(w, msg{
		Title: "접근 오류",
		Msg:   "접근권한이 부족합니다.",
	})
}
