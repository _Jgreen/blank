package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/securecookie"
)

//Clothes is
type Clothes struct {
	cookieHandler *securecookie.SecureCookie
}

//Newclothes is
func Newclothes() *Clothes {
	return &Clothes{
		cookieHandler: securecookie.New(
			securecookie.GenerateRandomKey(64),
			securecookie.GenerateRandomKey(32)),
	}
}

func (c *Clothes) setLoginCookie(u User, w http.ResponseWriter) {
	value := map[string]string{
		"no":   strconv.Itoa(u.No),
		"code": u.Code}
	if encoded, err := c.cookieHandler.Encode("account", value); err == nil {
		cookie := &http.Cookie{
			Name:  "account",
			Value: encoded,
			Path:  "/"}
		http.SetCookie(w, cookie)
	} else {
		//error
	}
}

func (c *Clothes) getLoginCookie(r *http.Request) *User {
	if cookie, err := r.Cookie("account"); err == nil {
		cookieValue := make(map[string]string)
		if err = c.cookieHandler.Decode("account", cookie.Value, &cookieValue); err == nil {
			if no, err2 := strconv.Atoi(cookieValue["no"]); err2 == nil {
				println("securecookie suc")
				return &User{
					No:   no,
					Code: cookieValue["code"],
				}
			}
		}
	}
	return &User{No: -1}
}

func (c *Clothes) gateway(w http.ResponseWriter, r *http.Request) {
	/*islogin, */ c.getLoginCookie(r)
}

func (s *Service) wearing(f func(w http.ResponseWriter, r *http.Request, u *User)) http.HandlerFunc {
	u := &User{No: -1, Auth: 9}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		usercookie := s.c.getLoginCookie(r)
		if usercookie.No != -1 {

			rows, err := s.db.Query(fmt.Sprintf(`SELECT no,id,name,code,point,level,auth FROM "users" WHERE no=%d`, usercookie.No))
			if err == nil {
				if rows.Next() { // 존재
					rows.Scan(&u.No, &u.Id, &u.Name, &u.Code, &u.Point, &u.Level, &u.Auth)
				}
			}
			defer rows.Close()
		}
		u.IsLogin = (u.No != -1 && u.No == usercookie.No && u.Code == usercookie.Code)
		f(w, r, u)
	})
}

/*
	sql 구문
	SELECT [columms] From "[tables]"
	INSERT INTO "[table]"([columms]...) VALUES ([칼럼에 맞는 값]...)
	UPDATE [table] SET [columms] = [값}]
	DELETE FROM [table]
*/
