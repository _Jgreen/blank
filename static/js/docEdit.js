newNode = () => {
    // 목록에서 추가
    var box = document.getElementsByClassName("toc-item-box")[0];
    var i = box.childElementCount;
    var obj = document.createElement("div");
    obj.setAttribute("class","toc-item");
    obj.setAttribute("id","t"+i);
    obj.innerHTML = `
    <a href="#h`+i+`" class="toc-anchor">`+i+`.</a>
    <input type="text" class="toc-text font-nanum" onkeydown="modifyItem(event)" onkeyup="modifyItem(event)">
    <button class="toc-item-remove" onclick="removeItem(event)">제거</button>
    `;
    box.appendChild(obj);

    // section 추가
    var main = document.getElementsByTagName("main")[0];
    var section = document.createElement("section");
    section.setAttribute("id","h"+i);
    section.innerHTML= `
    <h2>
        <a href="#t` + i + `">` + i + `.</a><span></span>
    </h2>
    <article class="article-content" contenteditable="true"></article>
    `
    main.appendChild(section);
};
newItem.onclick = newNode;
modifyItem = (e) => {
    var obj = e.target.parentElement;
    var val = e.target.value;
    var i = obj.id.slice(1,10);
    var header = document.getElementById("h"+i);
    header.children[0].getElementsByTagName("span")[0].innerText= val;
};

removeItem = (e) => {
    var obj = e.target.parentElement;
    var val = obj.children[0].innerText + obj.children[1].value;
    var i = obj.id.slice(1,10);
    var header = document.getElementById("h"+i);
    
    if (!confirm("정말로 '" + val + "'을 삭제시키겠습니까?")) { return 0; } else {
        obj.parentElement.removeChild(obj);
        header.parentElement.removeChild(header);
    } 
};
function newInputObj(name, value) {
    var obj = document.createElement("input");
    obj.setAttribute("type","hidden");
    obj.setAttribute("name",name);
    obj.value = value;
    return obj;
}

saveform.onsubmit = () => {
    var main = document.getElementsByTagName("main")[0];
    var count = main.childElementCount;
    saveform.appendChild(newInputObj("title",title.value));
    for(i=0; i<count; i++) {
        var t=main.children[i].children[0].children[1].innerText; // title
        var c=main.children[i].children[1].innerText; // contents
        saveform.appendChild(newInputObj("t",t));
        saveform.appendChild(newInputObj("c",c));
    }
}