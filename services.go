package main

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"

	"regexp"

	"github.com/gorilla/mux"
)

//Service is
type Service struct {
	r     *mux.Router
	tmpls map[string]*template.Template
	db    DB
	c     *Clothes
}

//NewService is
func NewService() *Service {
	return &Service{
		r:     mux.NewRouter(),
		tmpls: make(map[string]*template.Template),
		c:     Newclothes(),
	}
}

//SetupApp is
func (s *Service) SetupApp() {
	db, err := NewOpen()
	if err != nil {
		println(err.Error())
		return
	}
	s.db = db
	s.loadTemplate()
	s.chainApptoHandler()
}

func (s *Service) loadTemplate() {
	filepath.Walk("static\\template\\",
		func(path string, info os.FileInfo, err error) error {
			if m, _ := regexp.MatchString(".html", path); m {
				if t, err := template.ParseFiles(path); err == nil {
					s.tmpls[info.Name()[:len(info.Name())-5]] = t
				} else {
					println("load template error:", err.Error())
				}
			}
			return nil
		})

}

func (s *Service) chainApptoHandler() {
	s.r.HandleFunc("/", s.wearing(s.mainHandler))
	s.r.HandleFunc("/blank", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/", 302)
	})
	s.r.HandleFunc("/static/{type}/{file}", s.staticHandler)

	s.r.HandleFunc("/signin", s.signInPOSTMethod).Methods("POST")
	s.r.HandleFunc("/signin", s.signHandler)

	s.r.HandleFunc("/signup", s.signUpPOSTMethod).Methods("POST")
	s.r.HandleFunc("/signup", s.signHandler)
	s.r.HandleFunc("/signUp", s.checkIDMethod) // id 중복검사

	s.r.HandleFunc("/w", s.wearing(s.docSaveMethod)).Methods("POST")
	s.r.HandleFunc("/w", s.wearing(s.docEditHandler))

	s.r.HandleFunc("/p", s.wearing(s.projSaveHandler)).Methods("POST")
	s.r.HandleFunc("/p", s.wearing(s.projEditHandler)) // project proposal

	s.r.HandleFunc("/blank/{proj}", s.wearing(s.projViewHandler))
	s.r.HandleFunc("/blank/{proj}/{doc}", s.wearing(s.docViewHandler))
}
