package main

import (
	"fmt"
	"net/http"
)

//로그인 & 회원가입 Handler//
//로그인/회원가입 초기화면
func (s *Service) signHandler(w http.ResponseWriter, r *http.Request) {
	s.tmpls[r.URL.Path[1:]].Execute(w, nil)
}

// 로그인 요청
func (s *Service) signInPOSTMethod(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}
	id := r.FormValue("blankID")
	pass := r.FormValue("blankPASS")
	rows, err2 := s.db.Query(fmt.Sprintf(`SELECT no,id,pass,code,auth From "users" where id='%s';`, id))
	if err2 != nil {
		println("로그인 요청 :", err2.Error())
	}
	defer rows.Close()
	var u User
	for rows.Next() {
		if e := rows.Scan(&u.No, &u.Id, &u.Pass, &u.Code, &u.Auth); e != nil {
			println("알 수 없는 에러 :", e.Error())
			id = "1"
		}
	}
	if u.Pass != pass {
		fmt.Fprintf(w, `
		<script>
			alert("아이디와 비밀번호가 일치하지 않습니다.");
			location="signin";
		</script>
		`)
	} else {
		if u.Auth < 7 { // 쿠키 생성하고 메인으로
			s.c.setLoginCookie(u, w)
			fmt.Fprintf(w, `<script>location="/"</script>`)
		} else if u.Auth == 7 { // 회원가입 승인을 기다리고 있음
			obj := msg{
				Title: "가입승인 대기중입니다.",
				Msg:   fmt.Sprintf("@%s 는 가입 승인 대기 중입니다.", u.Id),
			}
			s.tmpls["msg"].Execute(w, obj)
		} else {
			obj := msg{
				Title: "가입 거절된 아이디입니다.",
				Msg:   fmt.Sprintf("@%s ms 가입신청이 거절되었습니다.", u.Id),
			}
			s.tmpls["msg"].Execute(w, obj)
		}
	}
}

type msg struct {
	Title string
	Msg   string
}

// 회원가입 id 중복 검사
func (s *Service) checkIDMethod(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	rows, err := s.db.Query(fmt.Sprintf(`SELECT id From "users" where id='%s';`, v.Get("id")))
	if err != nil {
		println("checkIdMethod: ", err.Error())
	}
	defer rows.Close()
	var id string
	for rows.Next() {
		if e := rows.Scan(&id); e != nil {
			println("알 수 없는 에러 :", e.Error())
			id = "1"
		}
	}
	if id != "" {
		fmt.Fprintf(w, `
		<script>
			alert("이미 존재하는 아이디입니다.");
			location="signup";
		</script>
		`)
	} else {
		obj := struct {
			Key string
		}{Key: v.Get("id")}
		s.tmpls["signup2"].Execute(w, obj)
	}

}

// 회원가입 요청
func (s *Service) signUpPOSTMethod(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}
	id := r.FormValue("ID")
	pass := r.FormValue("PASS")
	name := r.FormValue("REAL-NAME")
	code := r.FormValue("CODE")

	if _, err2 := s.db.Exec(fmt.Sprintf(`INSERT INTO "users"(id,pass,name,code) VALUES('%s','%s','%s','%s')`, id, pass, name, code)); err2 == nil {

		fmt.Fprintf(w, `
		<script>
			alert("회원가입 신청이 완료되었습니다.");
			location="signin";
		</script>
		`)
	} else {
		println("계정 생성 중 뭔가 에러 발생 ㅜ :", err2.Error())
	}
}

//Project & Docs handler//
//Board handler//
//Q&A handler//
