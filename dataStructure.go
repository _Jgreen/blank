package main

//User is
type User struct {
	No      int
	Id      string
	Pass    string
	Name    string
	Code    string
	Point   int
	Level   int
	Auth    int
	IsLogin bool
	// signup_date date
	// ok_date date
}

//ProjectUser is
type ProjectUser struct {
	U User
	W int
}

//Project a
type Project struct {
	User User
	No   int
	//birthday date
	//lastDate date
	Title    string
	Contents string
	Docs     []Doc
	Authors  []ProjectUser
	Write    int // 0-group 1-private 2-public
	Read     int // 0-group 1-private 2-public
}

//DocTmpl is
type DocTmpl struct {
	Title string
	Doc   Doc
	Docs  []Doc
}

//Doc a
type Doc struct {
	User     User
	No       int
	Title    string
	Contents []DocContents
	Parent   int
	//birthday date
	//lastDate date
}

//DocContents is
type DocContents struct {
	Title    string
	Contents string
}

// Post a
type Post struct{}

//Comment a
type Comment struct{}

//Question a
type Question struct{}

//Answer a
type Answer struct{}

//MainTmpl is
type MainTmpl struct {
	User  User
	P     int
	D     int
	U     int
	Projs []Project
}

/*
	sql 구문
	SELECT [columms] From "[tables]"
	INSERT INTO "[table]"([columms]...) VALUES ([칼럼에 맞는 값]...)
	UPDATE [table] SET [columms] = [값}]
	DELETE FROM [table]
*/

// ISUD is
type ISUD struct {
	query string
}

func (q *ISUD) i() {
	q.query = "INSERT"
}

func (q *ISUD) s() {
	q.query = "SELECT "
}

func (q *ISUD) u() {
	q.query = "UPDATE"
}

func (q *ISUD) d() {
	q.query = "DELETE"
}

var ql ISUD
